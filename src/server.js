const express = require('express')
const morgan = require('morgan')
const mongoosee = require('mongoose')

const app = express()
mongoosee.connect('mongodb://localhost/mevn-db', { useNewUrlParser: true })
    .then(db => console.log('DB is connected.'))
    .catch(err => console.error(err))

//Settings
app.set('port', process.env.PORT || 3000)

//Middlewares
app.use(morgan('dev'))
app.use(express.json())

//Routes
app.use('/api/tasks', require('./routes/tasks'))

//Static files (archivos que enviamos al front-end. El servidor los envia una vez y el cliente los consume)
app.use(express.static(__dirname + '/public'))

// Server listening
app.listen(app.get('port'), () => {
    console.log('Server on port', app.get('port'))
})