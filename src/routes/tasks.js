const express = require('express')
const router = express.Router()

const Task = require('../models/Task')
// Get all tasks
router.get('/', async (req, res) => {
    const tasks = await Task.find()
    res.json(tasks)
})
// Get a specific task
router.get('/:id', async (req, res) => {
    const task = await Task.findById(req.params.id)
        .catch(err => res.json('The element could not be found :('))
    res.json(task)
})
// Save a new task
router.post('/', async (req, res) => {
    const task = new Task(req.body)
    await task.save()
    res.json({
        status: 'Task saved.'
    })
})
// Update an existing task
router.put('/:id', async (req, res) => {
    await Task.findByIdAndUpdate(req.params.id, req.body)
    .catch(err => res.json('The element could not be found :('))
    res.json({status: 'Task updated.'})
})
// Delete an existing task
router.delete('/:id', async (req, res) => {
    await Task.findByIdAndDelete(req.params.id)
        .catch(err => res.json('The element could not be foxund :('))
    res.json({status: 'Task deleted.'})
})

module.exports = router