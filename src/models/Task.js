const mongoose = require('mongoose')
const { Schema } = mongoose

const Task = new Schema({
    title: String,
    description: String
})

//Convierto el esquema en un modelo para poder insertar, eliminar, actualizar, etc.
module.exports = mongoose.model('Task', Task)